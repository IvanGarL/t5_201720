package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import model.data_structures.DoubleLinkedList;
import model.data_structures.IList;
import model.data_structures.IndexMaxPQ;
import model.data_structures.List;
import model.data_structures.Ordenador;
import model.data_structures.StructuresIterator;
import model.exceptions.ElementNotFoundException;
import model.vo.ResumenTripVO;
import model.vo.RouteVO;
import model.vo.TripVO;
import api.ISTSManager;


public class STSManager implements ISTSManager{
	
	IList<TripVO> tripsList = new DoubleLinkedList<TripVO>();
	List<ResumenTripVO> resumenList = new List<ResumenTripVO>();
	List<RouteVO> routesList = new List<RouteVO>();
	IndexMaxPQ<RouteVO> routesPQ;
	

	public void loadTrips() 
	{
		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File("data/TXT/trips.txt")));
			reader.readLine();
			String line = reader.readLine();
			while( line != null )
			{
				String data[] = line.split(",");
				int routeId = Integer.parseInt(data[0]);
				int serviceId = Integer.parseInt(data[1]);
				int tripId = Integer.parseInt(data[2]);
				String headSing = data[3];
				String tripShortName = data[4];
				String directionId = data[5];
				int blockId = Integer.parseInt(data[6]);
				String shapeId = data[7];
				int wheelchairFriendly = Integer.parseInt(data[8]);
				int bikeAllowed = Integer.parseInt(data[9]);

				TripVO nuevo = new TripVO(routeId, serviceId, tripId, headSing, 
						tripShortName, directionId, blockId, shapeId, bikeAllowed, wheelchairFriendly);
				tripsList.add(nuevo);
				line = reader.readLine();
			}
			reader.close();
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		
		sortTripsList();
	}
	
	public void loadRoutes()
	{
		try {
			BufferedReader reader = new BufferedReader(new FileReader(new File("data/TXT/routes.txt")));
			reader.readLine();
			String line = reader.readLine();
			while( line != null )
			{
				String data[] = line.split(",");
				int id = Integer.parseInt(data[0]);
				String agencyId = data[1];
				String routeNum = data[2];
				String routeName = data[3];
				String routeDesc = data[4];
				String routeUrl = data[5];
				RouteVO nuevo = new RouteVO(id, agencyId, routeNum, routeName, routeDesc, routeUrl, null, null);
				routesList.add(nuevo);
				line = reader.readLine();
			}
			reader.close();

		} catch (Exception e) {
			// TODO: handle exception
		}	
		routesPQ = new IndexMaxPQ<RouteVO>(routesList.getSize());
	}
	
	RouteVO searchRoute(int routeId)throws ElementNotFoundException{
		RouteVO sch = null;
		StructuresIterator<RouteVO> iter = routesList.iterator();
		while(iter.getCurrentNode() != null){
			RouteVO actual = iter.getElement();
			if(actual.getId() == routeId) sch = iter.getElement();
			iter.next();
		}
		if(sch == null) throw new ElementNotFoundException("no se encontr� el la ruta buscada");
		return sch;
	}
	
	public void setRoutesPriority(){
		int priority = 0;
		StructuresIterator<TripVO> iter = tripsList.iterator();
		while(iter.getCurrentNode() != null){
			TripVO actual = iter.getElement();
			priority++;
			if(!actual.equals(tripsList.getLast())){
				TripVO next = iter.getCurrentNode().getNext().getElement();
				if(actual.getRouteId() != next.getRouteId()){
					try {
						RouteVO route = searchRoute(actual.getRouteId());
						route.setPriority(priority);
						priority = 0;
					} catch (ElementNotFoundException e) {
						//Wont happend
					}
				}
			}
			iter.next();
		}
	}
	
	public void routesToMaxPQ(){
		int i = 0, j = 0;
		for (i = 0; i < routesList.getSize(); i++) {
			routesPQ.insert(i, routesList.getElementAtK(i));
		}
		for (j = 0; j < routesPQ.maxIndex(); j++) {
			System.out.println("Route: " +  routesPQ.keyOf(j).getId() + " -- priority :" + routesPQ.keyOf(j).getPriority() );
		}
	}
	
	void sortTripsList(){
		TripVO arr[] = new TripVO[tripsList.getSize()];
		tripsList = tripsList.sort(arr);
//		for (int i = 0; i < tripsList.getSize(); i++) {
//			System.out.println(tripsList.getElementAtK(i).getBlockId());
//		}
	}
	
	public void loadResumeTrips(){
		List<Integer> ids =  new List<Integer>();
		StructuresIterator<TripVO> iter = tripsList.iterator();
		while(iter.getCurrentNode() != null){
			TripVO actual = iter.getElement();
			
			if(!actual.equals(tripsList.getLast())){
				TripVO next = iter.getCurrentNode().getNext().getElement();
				ids.add(actual.getTripId());
				if((actual.getBlockId() != next.getBlockId()) || (actual.getRouteId() != next.getRouteId())){
					int total = ids.getSize();
					Integer[] arr = new Integer[total];
					Integer[] trips = ids.toArray(arr);
					ResumenTripVO nuevo = new ResumenTripVO(actual.getBlockId(), actual.getRouteId(), total,trips);
					
					resumenList.add(nuevo);
					ids.restart();
				}
			}
			
			iter.next();
		}
		sortResumeSubTrips();
	}	
	
	void sortResumeSubTrips(){
		Ordenador<Integer>.HeapSort ord = new Ordenador<Integer>().new HeapSort();
		StructuresIterator<ResumenTripVO> iter = resumenList.iterator(); 
		while(iter.getCurrentNode() != null){
			ResumenTripVO actual = iter.getElement();
			ord.ordenarHeapSort(actual.getTrips());
			iter.next();
		}
	}
}
