package model.vo;

public class TripVO implements Comparable<TripVO>
{
	private int routeId;
	private int serviceId;
	private int  tripId;
	private String headSing;
	private String tripShortName;
	private String directionId;
	private int blockId;
	private String shapeId;
	private boolean bikeAllowed;
	private boolean wheelchairFriendly;
	
	public TripVO(int pRouteID, int pSID, int pTID, String pHS, String pTSN, String pDID, int pBID, String pSHID, int bike, int wheel)
	{
		routeId = pRouteID;
		serviceId = pSID;
		tripId = pTID;
		headSing = pHS;
		tripShortName = pTSN;
		directionId = pDID;
		blockId = pBID;
		shapeId = pSHID;
		if( bike == 0 ) bikeAllowed = false; else bikeAllowed = true;
		if( wheel == 0 ) wheelchairFriendly = false; else wheelchairFriendly = true;
	}

	public int getRouteId() {
		return routeId;
	}

	public void setRouteId(int routeId) {
		this.routeId = routeId;
	}

	public int getServiceId() {
		return serviceId;
	}

	public void setServiceId(int serviceId) {
		this.serviceId = serviceId;
	}

	public int getTripId() {
		return tripId;
	}

	public void setTripId(int tripId) {
		this.tripId = tripId;
	}

	public String getHeadSing() {
		return headSing;
	}

	public void setHeadSing(String headSing) {
		this.headSing = headSing;
	}

	public String getTripShortName() {
		return tripShortName;
	}

	public void setTripShortName(String tripShortName) {
		this.tripShortName = tripShortName;
	}

	public String getDirectionId() {
		return directionId;
	}

	public void setDirectionId(String directionId) {
		this.directionId = directionId;
	}

	public int getBlockId() {
		return blockId;
	}

	public void setBlockId(int blockId) {
		this.blockId = blockId;
	}

	public String getShapeId() {
		return shapeId;
	}

	public void setShapeId(String shapeId) {
		this.shapeId = shapeId;
	}

	public boolean isBikeAllowed() {
		return bikeAllowed;
	}

	public void setBikeAllowed(boolean bikeAllowed) {
		this.bikeAllowed = bikeAllowed;
	}

	public boolean isWheelchairFriendly() {
		return wheelchairFriendly;
	}

	public void setWheelchairFriendly(boolean wheelchairFriendly) {
		this.wheelchairFriendly = wheelchairFriendly;
	}
	public String toString()
	{
		return "" + tripId;
	}

	@Override
	public int compareTo(TripVO aTrip) {
		
		if(getBlockId() > aTrip.getBlockId()) return 1;
		else if(getBlockId() < aTrip.getBlockId()) return -1;
		else return 0;
	}
}
