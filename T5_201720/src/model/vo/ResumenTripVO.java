package model.vo;

public class ResumenTripVO implements Comparable<ResumenTripVO>{

	public int blockId;
	public int routeId;
	public int totalBlocks;
	public Integer[] trips;
	
	public ResumenTripVO(int pBlock, int pRoute, int pTotal, Integer[] pTrips){
		blockId = pBlock;
		routeId = pRoute;
		totalBlocks = pTotal;
		trips = pTrips;
	}
	
	public int getBlockId(){
		return blockId;
	}
	
	public int getRouteId(){
		return routeId;
	}
	
	public int getTotalBlocks(){
		return totalBlocks;
	}
	
	public Integer[] getTrips(){
		return trips;
	}
	@Override
	public int compareTo(ResumenTripVO o) {
		// TODO Auto-generated method stub
		return 0;
	}



}
