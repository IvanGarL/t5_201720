package model.vo;

import java.awt.Color;

public class RouteVO implements Comparable<RouteVO>
{	
	private int id;
	private String agencyId;
	private String routeNum;
	private String routeName;
	private String routeDesc;
	private String routeUrl;
	private Color routeColor;
	private Color routeTextColor;
	private String agencyName;
	private int numberOfStops;
	
	private int priority;
	
	public RouteVO( int pId, String pAngencyId, String pRouteNum, String pRouteName, String pRouteDesc, String pUrl, Color pRouteColor, Color pRouteText)
	{
		id = pId;
		agencyId = pAngencyId;
		routeDesc = pRouteDesc;
		routeColor = pRouteColor;
		routeTextColor = pRouteText;
		routeUrl = pUrl;
		routeNum = pRouteNum;
		routeName = pRouteName;
		priority = 0;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAgencyId() {
		return agencyId;
	}

	public void setAgencyId(String agencyId) {
		this.agencyId = agencyId;
	}

	public String getRouteNum() {
		return routeNum;
	}

	public void setRouteNum(String routeNum) {
		this.routeNum = routeNum;
	}

	public String getRouteName() {
		return routeName;
	}

	public void setRouteName(String routeName) {
		this.routeName = routeName;
	}

	public String getRouteDesc() {
		return routeDesc;
	}

	public void setRouteDesc(String routeDesc) {
		this.routeDesc = routeDesc;
	}

	public String getRouteUrl() {
		return routeUrl;
	}

	public void setRouteUrl(String routeUrl) {
		this.routeUrl = routeUrl;
	}

	public Color getRouteColor() {
		return routeColor;
	}

	public void setRouteColor(Color routeColor) {
		this.routeColor = routeColor;
	}

	public Color getRouteTextColor() {
		return routeTextColor;
	}

	public void setRouteTextColor(Color routeTextColor) {
		this.routeTextColor = routeTextColor;
	}
	public String toString()
	{
		return id + "";
	}

	public String getAgencyName()
	{
		return agencyName;
	}
	public void setAgencyName(String pName)
	{
		agencyName = pName;
	}

	public int getPriority(){
		return priority;
	}
	public void setPriority(int x){
		priority = x;
	}
	
	@Override
	public int compareTo(RouteVO r) {
		int comp = getPriority() - r.getPriority();
		if(comp > 0) return 1;
		else if(comp < 0) return -1;
		else return 0;
	}
	public int compareTo(int tam)
	{
		if(numberOfStops < tam )
			return -1;
		else if( numberOfStops > tam)
			return 1;
		else
			return 0;
	}
}

