package model.data_structures;

import java.util.Comparator;

public class Ordenador<T extends Comparable<T>>
{
	
	/*
	 * MERGESORT
	 */
	public class MergeSort{
	
			
		void merge(Comparable[] a, Comparable[] aux, int lo, int mid, int hi){
			for (int k = lo; k <= hi ; k++) {
				aux[k] = a[k];
			}
			
			int i = lo, j = mid+1;
			for (int k = lo; k <= hi; k++) {
				if(i > mid) a[k] = aux[j++];
				else if(j > hi) a[k] = aux[i++];
				else if(aux[j].compareTo(aux[i]) < 0) a[k] = aux[j++];
				else a[k] = aux[i++];
			}
		}
		void mergeSort(Comparable a[], Comparable[] aux, int lo, int hi){
			
			if(hi <= lo) return;
			int mid = lo + (hi - lo) / 2;
			 mergeSort(a, aux, lo, mid);
			 mergeSort(a, aux, mid+1, hi);
			 merge(a, aux, lo, mid, hi); 
			
		}
		public void ordenarMergeSort(Comparable[] a){
			 Comparable[] aux = new Comparable[a.length];
			 mergeSort(a, aux, 0, a.length - 1); 
		}
	
		public int compare(T a, T b) {
			
			return a.compareTo(b);
		}
	}
	
	
	/*
	 * HEAP-SORT
	 */
	public class HeapSort{
		
		private Comparable[] a;
	    private int n;
	    private int left;
	    private int right;
	    private int largest;

	    
	    public void buildHeap(Comparable []a){
	        n = a.length-1;
	        for(int i = n/2; i >= 0; i--){
	            maxHeap(a,i);
	        }
	    }
	    
	    public void maxHeap(Comparable[] a, int i){ 
	        left=2*i;
	        right=2*i+1;
	        
	        if(left <= n && (a[left].compareTo(a[i]) > 0)){
	            largest = left;
	        }
	        
	        else{
	            largest = i;
	        }
	        
	        if(right <= n && (a[right].compareTo(a[largest]) > 0)){
	            largest = right;
	        }
	        
	        if(largest != i){
	            exchange(i,largest);
	            maxHeap(a, largest);
	        }
	    }
	    
	    public void exchange(int i, int j){
	        Comparable t = a[i];
	        a[i] = a[j];
	        a[j]=t; 
	        }
	    
	    public void ordenarHeapSort(Comparable[] arr){
	        a = arr;
	        buildHeap(a);
	        
	        for(int i = n; i>0 ;i--){
	            exchange(0, i);
	            n = n-1;
	            maxHeap(a, 0);
	        }
	    }
	    
	    public int compare(T a, T b) {
			
			return a.compareTo(b);
		}
	}
}
