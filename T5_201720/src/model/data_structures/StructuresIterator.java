package model.data_structures;


public class StructuresIterator<T>
{

	protected Node<T> current;
	
	public StructuresIterator(Node<T> first)
	{
		current = first;
	}
	public boolean hasNext() {
		// TODO Auto-generated method stub
		return current.hasNext();
	}

	public void next() {
		// TODO Auto-generated method stub
		current = current.getNext();
	}
	public void previous()
	{
		current = current.getPrevious();
	}
	public boolean hasPrevious()
	{
		return current.getPrevious() != null;
	}

	public Node<T> getCurrentNode(){
		return current;
	}
	public T getElement()
	{
		return current.getElement();
	}
	public boolean contains(T elem)
	{
		if( current == null )
		{
			return false;
		}
		while( hasNext() )
		{
			if(getElement().equals(elem))
			{
				return true;
			}
			else
			{				
				next();
			}
		}
		return false;
	}

}
