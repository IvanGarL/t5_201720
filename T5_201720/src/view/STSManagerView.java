package view;

import java.util.Scanner;

import model.data_structures.StructuresIterator;
import model.exceptions.ElementNotFoundException;
import controller.Controller;

public class STSManagerView {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		boolean fin=false;
		while(!fin){
			printMenu();
			
			int option = sc.nextInt();
			
			switch(option){
				case 1:
					double startTime = System.currentTimeMillis();
					Controller.loadRoutes();
					Controller.loadTrips();
					Controller.setRoutesPriority();
					double endTime = System.currentTimeMillis();
					System.out.println("Se han gastado " + (endTime - startTime)/1000 + " segundos.");

					break;
				case 2:
					double startTime2 = System.currentTimeMillis();
					Controller.loadResumeTrips();
					double endTime2 = System.currentTimeMillis();
					System.out.println("Se han gastado " + (endTime2 - startTime2)/1000 + " segundos.");
					break;
				case 3:
					double startTime3 = System.currentTimeMillis();
					Controller.routesToMaxPQ();
					double endTime3 = System.currentTimeMillis();
					System.out.println("Se han gastado " + (endTime3 - startTime3)/1000 + " segundos.");
					break;
				case 4:	
					fin=true;
					sc.close();
					break;
			}
		}
	}

	private static void printMenu() {
		System.out.println("---------ISIS 1206 - Estructuras de datos----------");
		System.out.println("---------------------Taller 3----------------------");
		System.out.println("1. Cargar y ordenar Trips (Mergesort) & Cargar Rutas(con prioridad)");
		System.out.println("2. Cargar Resumen de Trips & ordenar (Heapsort)");
		System.out.println("3. Routes Priority Queue");
		System.out.println("4. Salir");
		System.out.println("Type the option number for the task, then press enter: (e.g., 1):");
		
	}
}
