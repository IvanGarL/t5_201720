package api;

import java.io.File;


public interface ISTSManager {

	/**
	 * Method to load the trips of the STS
	 * @param tripsFile - path to the file 
	 */
	public void loadTrips();	
		
	public void loadResumeTrips();
	
	public void loadRoutes();
	
	public void setRoutesPriority();
	
	public void routesToMaxPQ();

}
