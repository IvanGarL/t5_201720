package controller;

import java.io.File;

import model.exceptions.ElementNotFoundException;
import model.logic.STSManager;
import api.ISTSManager;

public class Controller {
	
	/**
	 * Reference to the routes and stops manager
	 */
	private static ISTSManager  manager = new STSManager();

	public static void loadTrips() {
		manager.loadTrips();
	}
	
	public static void loadResumeTrips(){
		manager.loadResumeTrips();
	}
	
	public static void loadRoutes(){
		manager.loadRoutes();
	}

	public static void setRoutesPriority(){
		manager.setRoutesPriority();
	}
	
	public static void routesToMaxPQ(){
		manager.routesToMaxPQ();
	}
}
